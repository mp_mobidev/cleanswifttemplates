//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

//MARK: - Boundary protocols
protocol ___VARIABLE_sceneName:identifier___DataPassing {
    var dataStore: ___VARIABLE_sceneName:identifier___DataStore? {get set}
}

// MARK: - Class
/**
    Class to route to new view controller when it's needed to display something that current view controller is not responsible for.
 */
class ___VARIABLE_sceneName:identifier___Router: NSObject, ___VARIABLE_sceneName:identifier___DataPassing {
    weak var viewController: ___VARIABLE_sceneName:identifier___ViewController?
    var dataStore: ___VARIABLE_sceneName:identifier___DataStore?
    
    // MARK: Manual navigation
    @objc func goToSomewhereViewController(segue: UIStoryboardSegue?) {
        //        let destinationVC = segue?.destination as! SomewhereViewController
        //        var destinationDS = destinationVC.router!.dataStore!
        //        self.passDataToGifs(source: dataStore!, destination: &destinationDS)
    }
    
//    @objc func navigateToSomewhere() {
        // NOTE: Teach the router how to navigate to another scene. Some examples follow:
        
        // 1. Trigger a storyboard segue
        // viewController?.performSegue(withIdentifier: "ShowSomewhereScene", sender: nil)
        
        // 2. Present another view controller programmatically
        // viewController?.present(someWhereViewController, animated: true, completion: nil)
        
        // 3. Ask the navigation controller to push another view controller onto the stack
        // viewController?.navigationController?.pushViewController(someWhereViewController, animated: true)
        
        // 4. Present a view controller from a different storyboard
        // let storyboard = UIStoryboard(name: "OtherThanMain", bundle: nil)
        // let someWhereViewController = storyboard.instantiateInitialViewController() as! SomeWhereViewController
        // viewController?.navigationController?.pushViewController(someWhereViewController, animated: true)
//    }
    
    
}

//MARK: - private section
fileprivate extension ___VARIABLE_sceneName:identifier___Router {
    //   MARK: Passing data
//    func passDataToDestination(source: ___VARIABLE_sceneName:identifier___DataStore, destination: inout DestinationDataStore){
//        destination.data = source.data
//    }
    
    
}
